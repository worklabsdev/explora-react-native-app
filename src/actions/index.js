import globalConst from "../helpers/global";
export const LOGIN_USER_BEGIN = "LOGIN_USER_BEGIN";
export const LOGIN_USER_SUCCESS = "LOGIN_USER_SUCCESS";
export const LOGIN_USER_INVALID = "LOGIN_USER_INVALID";
export const LOGIN_USER_ERROR = "LOGIN_USER_ERROR";

export const REGISTER_USER_BEGIN = "REGISTER_USER_BEGIN";
export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const REGISTER_USER_ERROR = "REGISTER_USER_ERROR";

export const GET_INTEREST_BEGIN = "GET_INTEREST_BEGIN";
export const GET_INTEREST_SUCCESS = "GET_INTEREST_SUCCESS";
export const GET_INTEREST_ERROR = "GET_INTEREST_ERROR";

export const SAVE_ACTIVITY_BEGIN = "SAVE_ACTIVITY_BEGIN";
export const SAVE_ACTIVITY_SUCCESS = "SAVE_ACTIVITY_SUCCESS";
export const SAVE_ACTIVITY_ERROR = "SAVE_ACTIVITY_ERROR";

export const DELETE_ACTIVITY_BEGIN = "DELETE_ACTIVITY_BEGIN";
export const DELETE_ACTIVITY_SUCCESS = "DELETE_ACTIVITY_SUCCESS";
export const DELETE_ACTIVITY_ERROR = "DELETE_ACTIVITY_ERROR";

export const LIST_ACTIVITIES_BEGIN = "LIST_ACTIVITIES_BEGIN";
export const LIST_ACTIVITIES_SUCCESS = "LIST_ACTIVITIES_SUCCESS";
export const LIST_ACTIVITIES_ERROR = "LIST_ACTIVITIES_ERROR";

export const EDIT_PROFILE_BEGIN = "EDIT_PROFILE_BEGIN";
export const EDIT_PROFILE_SUCCESS = "EDIT_PROFILE_SUCCESS";
export const EDIT_PROFILE_ERROR = "EDIT_PROFILE_ERROR";

export const EDIT_INTEREST_BEGIN = "EDIT_INTEREST_BEGIN";
export const EDIT_INTEREST_SUCCESS = "EDIT_INTEREST_SUCCESS";
export const EDIT_INTEREST_ERROR = "EDIT_INTEREST_ERROR";

export const GET_MYACTIVITIES_BEGIN = "GET_MYACTIVITIES_BEGIN";
export const GET_MYACTIVITIES_SUCCESS = "GET_MYACTIVITIES_SUCCESS";
export const GET_MYACTIVITIES_ERROR = "GET_MYACTIVITIES_ERROR";

export const GET_SINGLE_ACTIVITY_BEGIN = "GET_SINGLE_ACTIVITY_BEGIN";
export const GET_SINGLE_ACTIVITY_SUCCESS = "GET_SINGLE_ACTIVITY_SUCCESS";
export const GET_SINGLE_ACTIVITY_ERROR = "GET_SINGLE_ACTIVITY_ERROR";

export const APPLY_ACTIVITY_BEGIN = "APPLY_ACTIVITY_BEGIN";
export const APPLY_ACTIVITY_SUCCESS = "APPLY_ACTIVITY_SUCCESS";
export const APPLY_ACTIVITY_ERROR = "APPLY_ACTIVITY_ERROR";

export const GET_MYREQUESTS_BEGIN = "GET_MYREQUESTS_BEGIN";
export const GET_MYREQUESTS_SUCCESS = "GET_MYREQUESTS_SUCCESS";
export const GET_MYREQUESTS_ERROR = "GET_MYREQUESTS_ERROR";

export const LOGOUT_CLEAR_SUCCESS = "LOGOUT_CLEAR_SUCCESS";

export const login_begin = () => ({
  type: LOGIN_USER_BEGIN
});

export const login_success = data => ({
  type: LOGIN_USER_SUCCESS,
  data
});
export const login_invalid = data => ({
  type: LOGIN_USER_INVALID,
  data
});
export const login_error = error => ({
  type: LOGIN_USER_ERROR,
  error
});

export const login_user = data => {
  return dispatch => {
    dispatch(login_begin());
    // console.warn("this is the data", data);
    // globalConst.serverip +
    console.log(globalConst.serverip);
    fetch(`${globalConst.serverip}users/authenticate`, {
      body: JSON.stringify(data),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        console.log("dataaa" + JSON.parse(JSON.stringify(dat)).status);
        var dd = JSON.parse(JSON.stringify(dat));
        console.log("htis is teh status", dd.status);
        if (dd.status == 200) {
          console.log("this is data return ", dd);
          dispatch(login_success(dd));
        }
        if (dd.status >= 400) {
          // Toast.show({
          //   text: "Invalid Creds !",
          //   buttonText: "Okay",
          //   buttonTextStyle: { color: "#FFF" },
          //   buttonStyle: { backgroundColor: "#FF0000" },
          //   textStyle: { color: "red" },
          //   style: { backgroundColor: "white" },
          //   duration: 3000
          // });
          console.log("INVALID CREDS", dd);
          dispatch(login_invalid(dd));
        }
        // else {
        //   dispatch(invalid_login(dat));
        // }
        return dat;
      })
      .catch(err => {
        // if()
        console.log("rerorrrrrr", err);
        dispatch(login_error(err));
        console.log("error" + err);
      });
  };
};

//  ====================== Register Actions ========================

export const register_begin = () => ({
  type: REGISTER_USER_BEGIN
});
export const register_success = data => ({
  type: REGISTER_USER_SUCCESS,
  data
});
export const register_error = error => ({
  type: REGISTER_USER_ERROR,
  error
});

export const register_user = data => {
  return dispatch => {
    dispatch(register_begin());
    console.log("this is the data", data);
    fetch(`${globalConst.serverip}users/register`, {
      body: JSON.stringify(data),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(dat => dat.json())
      .then(dat => {
        console.log("data got after saving", dat);
        var dd = JSON.parse(JSON.stringify(dat));
        if (dd.status == 200) {
          console.log("this is data register ", dat);
          dispatch(register_success(dat));
        }
        // return dat;
      })
      .catch(err => {
        console.log("ERORRRRRRRRRR", err);
        dispatch(register_error(err));
      });
  };
};

export const get_interest_begin = () => ({
  type: GET_INTEREST_BEGIN
});

export const get_interest_success = data => ({
  type: GET_INTEREST_SUCCESS,
  data
});

export const get_interest_error = error => ({
  type: GET_INTEREST_ERROR,
  error
});


export const save_activity_begin = () => ({
  type: SAVE_ACTIVITY_BEGIN
});

export const save_activity_success = data => ({
  type: SAVE_ACTIVITY_SUCCESS,
  data
});

export const save_activity_error = error => ({
  type: SAVE_ACTIVITY_ERROR,
  error
});

export const save_activity = dat => {
  return dispatch => {
    console.log("you are in action ", dat);
    dispatch(save_activity_begin());
    // `${globalConst.serverip}
    fetch(`${globalConst.serverip}act/addactivity`, {
      body: JSON.stringify(dat),
      method: "post",
      headers: { "Content-Type": "application/json" }
    })
      .then(data => data.json())
      .then(data => {
        console.log("DATA AFTE SAVING", data);
        var dd = JSON.parse(JSON.stringify(data));
        if (dd.status == 200) {
          console.log("GOT 200" + dat.userId);
          dispatch(getMyActivities(dat.userId));
          dispatch(save_activity_success(dd));
        }
      })
      .catch(error => {
        console.log("ERROR IT IS", error);
        dispatch(save_activity_error(error));
      });
  };
};


export const list_activities = id => {
  return dispatch => {
    dispatch(list_activities_begin());
    fetch(`${globalConst.serverip}act/gettingactivities/${id}`)
      .then(data => data.json())
      .then(data => {
        console.log("Getting data from this ", data.data);
        var dd = data.data;
        if (data.status == 200) {
          console.log("this is the status _______________", data);
          dispatch(list_activities_success(dd));
        }
      })
      .catch(error => {
        console.log("this is the error ", error);
        dispatch(list_activities_error(error));
      });
  };
};

export const deleteActivityBegin = () => ({
  type: DELETE_ACTIVITY_BEGIN
});


export const deleteActivity = (aid, uid) => {
  // console.log("USERIDIDIDIDIDIDID", id);
  return dispatch => {
    dispatch(deleteActivityBegin());
    fetch(`${globalConst.serverip}act/addignorantuser/${aid}/${uid}`, {
      method: "get",
      headers: { "Content-Type": "application/json" }
    })
      .then(data => data.json())
      .then(data => {
        if (data.status == 200) {
          dispatch(deleteActivitySuccess());
          dispatch(list_activities(uid));
        } else if (data.status <= 400) {
          console.log("got error adding ignorant user");
        }
      })
      // .then(dispatch(deleteActivitySuccess()))
      // .then(dispatch(list_activities(uid)))
      .catch(err => dispatch(deleteActivityError(err)));
  };
};

const editProfileBegin = () => ({
  type: EDIT_PROFILE_BEGIN,
  loading: true
});
const editProfileSucess = () => ({
  type: EDIT_PROFILE_SUCCESS,
  loading: false
});
const editProfileFail = () => ({
  type: EDIT_PROFILE_ERROR,
  loading: false
});

export const editProfile = (data, id) => {
  console.log("IDDDDDDD", id);
  console.log("DATAATATATATA", data);
  const { name, bio, location } = data;
  return dispatch => {
    dispatch(editProfileBegin());
    fetch(`${globalConst.serverip}users/editpersonalinfo/${id}`, {
      method: "put",
      name,
      location,
      bio,
      headers: { "Content-Type": "application/json" }
    })
      .then(data => dispatch(editProfileSucess(data)))
      .catch(err => dispatch(editProfileFail(err)));
  };
};

const editInterestsBegin = () => ({
  type: EDIT_INTEREST_BEGIN,
  loading: true
});
const editInterestsSucess = data => ({
  type: EDIT_INTEREST_SUCCESS,
  loading: false,
  data
});
const editInterestsFail = err => ({
  type: EDIT_INTEREST_ERROR,
  err,
  loading: false
});



export const getmyactivities_begin = () => ({
  type: GET_MYACTIVITIES_BEGIN
});
export const getmyactivities_success = data => ({
  type: GET_MYACTIVITIES_SUCCESS,
  data
});
export const getmyactivities_error = error => ({
  type: GET_MYACTIVITIES_ERROR,
  error
});

export const getMyActivities = id => {
  console.log("GETTING USERID", id);
  return dispatch => {
    dispatch(getmyactivities_begin());
    fetch(`${globalConst.serverip}act/getsingleuseractivities/${id}`)
      .then(data => data.json())
      .then(data => {
        console.log("Getting data from this ", data.data);
        var dd = data.data;
        if (data.status == 200) {
          console.log("this is the status _______________", data);
          dispatch(getmyactivities_success(dd));
        }
      })
      .catch(error => {
        console.log("this is the error ", error);
        dispatch(getmyactivities_error(error));
      });
  };
};



export const applyActivity = (aid, uid) => {
  return dispatch => {
    dispatch(applyActivityBegin());
    fetch(`${globalConst.serverip}act/applyforactivity/${aid}/${uid}`, {
      method: "get",
      headers: { "Content-Type": "application/json" }
    })
      .then(data => data.json())
      .then(data => {
        console.log("BEFORE DATA", data);
        if (data.status == 200) {
          console.log("DATATATATATATATA333", data.data._id);
          dispatch(applyActivitySuccess(data));
          dispatch(list_activities(uid));
        } else if (data.status <= 400) {
          console.log("got error applying to activity");
        }
      })
      // .then(dispatch(applyActivitySuccess()))/
      // .then(dispatch(list_activities(uid)))
      .catch(err => dispatch(applyActivityError(err)));
  };
};
export const getsingleActivity_begin = () => ({
  type: GET_SINGLE_ACTIVITY_BEGIN
});
export const getsingleActivity_success = data => ({
  type: GET_SINGLE_ACTIVITY_SUCCESS,
  data
});
export const getsingleActivity_error = error => ({
  type: GET_SINGLE_ACTIVITY_ERROR,
  error
});


export const logoutClear_success = () => ({
  type: LOGOUT_CLEAR_SUCCESS
});

export const logoutClear = () => {
  return dispatch => {
    dispatch(logoutClear_success);
  };
};

