import {
  LOGIN_USER_BEGIN,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ERROR,
  LOGIN_USER_INVALID,
  REGISTER_USER_BEGIN,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
  EDIT_PROFILE_BEGIN,
  EDIT_PROFILE_SUCCESS,
  EDIT_PROFILE_ERROR,
  LOGOUT_CLEAR_SUCCESS
} from "../actions";

const initialState = {
  userLoggedIn: false,
  userloginLoading: false,
  regLoading: false,
  registerSuccess: false,
  loginData: {}
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_USER_BEGIN:
      return {
        ...state,
        userloginLoading: true,
        userLoggedIn: false
      };
    case LOGIN_USER_SUCCESS:
      console.log(
        "this is the user data++++++++++++++++++++++++",
        JSON.parse(JSON.stringify(action.data)).response.token
      );
      // if(action.data.status==200){

      // }
      return {
        ...state,
        userLoggedIn: true,
        userloginLoading: false,
        loginData: action.data
      };
    case LOGIN_USER_ERROR:
      console.log(
        "this is the user error++++++++++++++++++++++++",
        action.data
      );
      return {
        ...state,
        userLoggedIn: false,
        userloginLoading: false,
        loginData: action.data
      };
    case LOGIN_USER_INVALID:
      console.log("this is the invalid data action " + action.data);
      return {
        ...state,
        userLoggedIn: false,
        userloginLoading: false,
        loginData: action.data
      };
    case REGISTER_USER_BEGIN:
      return {
        ...state,
        regLoading: true
      };
    case REGISTER_USER_SUCCESS:
      return {
        ...state,
        regLoading: false,
        registerSuccess: true,
        loginData: action.data
      };
    case REGISTER_USER_ERROR:
      return {
        ...state,
        regLoading: false
      };
    case LOGOUT_CLEAR_SUCCESS:
      return {
        ...state,
        userLoggedIn: false,
        loginData: {}
      };
    default:
      return state;
  }
};

export default user;
