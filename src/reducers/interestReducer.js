import {
  GET_INTEREST_BEGIN,
  GET_INTEREST_SUCCESS,
  GET_INTEREST_ERROR,
  EDIT_INTEREST_BEGIN,
  EDIT_INTEREST_SUCCESS,
  EDIT_INTEREST_ERROR
} from "../actions";

const initialState = {
  getInterestLoading: false,
  interestData: [],
  editinterestLoading: false,
  successData: ""
};

const interest = (state = initialState, action) => {
  switch (action.type) {
    case GET_INTEREST_BEGIN:
      return {
        ...state,
        getInterestLoading: true
      };
    case GET_INTEREST_SUCCESS:
      return {
        ...state,
        getInterestLoading: false,
        interestData: action.data
      };
    case GET_INTEREST_ERROR:
      return {
        ...state,
        getInterestLoading: false
      };
    case EDIT_INTEREST_BEGIN:
      return {
        ...state,
        editinterestLoading: true
      };
    case EDIT_INTEREST_SUCCESS:
      return {
        ...state,
        editinterestLoading: true,
        successData: action.data
      };
    case EDIT_INTEREST_ERROR:
      return {
        ...state,
        editinterestLoading: true
      };
    default:
      return state;
  }
};

export default interest;
