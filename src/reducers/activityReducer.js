import {
  SAVE_ACTIVITY_BEGIN,
  SAVE_ACTIVITY_SUCCESS,
  SAVE_ACTIVITY_ERROR,
  LIST_ACTIVITIES_BEGIN,
  LIST_ACTIVITIES_SUCCESS,
  LIST_ACTIVITIES_ERROR,
  DELETE_ACTIVITY_BEGIN,
  DELETE_ACTIVITY_SUCCESS,
  DELETE_ACTIVITY_ERROR,
  GET_MYACTIVITIES_BEGIN,
  GET_MYACTIVITIES_SUCCESS,
  GET_MYACTIVITIES_ERROR,
  APPLY_ACTIVITY_BEGIN,
  APPLY_ACTIVITY_SUCCESS,
  APPLY_ACTIVITY_ERROR,
  GET_SINGLE_ACTIVITY_BEGIN,
  GET_SINGLE_ACTIVITY_SUCCESS,
  GET_SINGLE_ACTIVITY_ERROR,
  GET_MYREQUESTS_BEGIN,
  GET_MYREQUESTS_ERROR,
  GET_MYREQUESTS_SUCCESS
} from "../actions";

const initialState = {
  saveActivityLoading: false,
  savedActivity: false,
  activitiesData: [],
  myactivityData: [],
  listActivitiesLoading: false,
  applyactivityLoading: false,
  applySuccessData: {},
  ignoreSuccessData: {},
  getsingleactivityLoading: false,
  singleactivityData: {},
  getmyrequestsLoading: false,
  myrequestsData: {}
};

const activity = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_ACTIVITY_BEGIN:
      return {
        ...state,
        saveActivityLoading: true
      };
    case SAVE_ACTIVITY_SUCCESS:
      return {
        ...state,
        saveActivityLoading: false,
        savedActivity: true,
        activitiesData: action.data
      };
    case SAVE_ACTIVITY_ERROR:
      return {
        ...state,
        saveActivityLoading: false
      };
    case LIST_ACTIVITIES_BEGIN:
      return {
        ...state,
        listActivitiesLoading: true
      };
    case LIST_ACTIVITIES_SUCCESS:
      return {
        ...state,
        activitiesData: action.data,
        listActivitiesLoading: false
      };
    case LIST_ACTIVITIES_ERROR:
      return {
        ...state,
        listActivitiesLoading: true
      };
    case DELETE_ACTIVITY_BEGIN:
      return {
        ...state,
        listActivitiesLoading: true
      };
    case DELETE_ACTIVITY_SUCCESS:
      return {
        ...state,
        listActivitiesLoading: false
      };
    case DELETE_ACTIVITY_ERROR:
      return {
        ...state,
        listActivitiesLoading: false,
        error: action.error
      };
    case GET_MYACTIVITIES_BEGIN:
      return {
        ...state,
        listActivitiesLoading: true
      };
    case GET_MYACTIVITIES_SUCCESS:
      return {
        ...state,
        listActivitiesLoading: false,
        myactivityData: action.data
      };
    case GET_MYACTIVITIES_ERROR:
      return {
        ...state,
        listActivitiesLoading: false,
        error: action.error
      };
    case APPLY_ACTIVITY_BEGIN:
      return {
        ...state,
        applyactivityLoading: true
      };
    case APPLY_ACTIVITY_SUCCESS:
      return {
        ...state,
        applyactivityLoading: false,
        applySuccessData: action.data
      };
    case APPLY_ACTIVITY_ERROR:
      return {
        ...state,
        applyactivityLoading: false,
        error: action.error
      };
    case GET_SINGLE_ACTIVITY_BEGIN:
      return {
        ...state,
        getsingleactivityLoading: true
      };
    case GET_SINGLE_ACTIVITY_SUCCESS:
      return {
        ...state,
        getsingleactivityLoading: true,
        singleactivityData: action.data
      };
    case GET_SINGLE_ACTIVITY_ERROR:
      return {
        ...state,
        getsingleactivityLoading: true,
        error: action.error
      };
    case GET_MYREQUESTS_BEGIN:
      return {
        ...state,
        getmyrequestsLoading: true
      };
    case GET_MYREQUESTS_SUCCESS:
      return {
        ...state,
        getmyrequestsLoading: false,
        myrequestsData: action.data
      };
    case GET_MYREQUESTS_ERROR:
      return {
        ...state,
        getmyrequestsLoading: false
      };

    default:
      return state;
  }
};

export default activity;
