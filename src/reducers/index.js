import { combineReducers } from "redux";
import user from "./userReducer";
import interest from "./interestReducer";
import activity from "./activityReducer";
export default combineReducers({ user, interest, activity });
