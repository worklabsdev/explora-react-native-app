import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  View,
  StatusBar,
  TextInput,
  ScrollView,
  TouchableOpacity
} from "react-native";
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Button,
  Icon,
  Fab,
  Text,
  Toast
} from "native-base";
import { register_user } from "../actions";
import Spinner from "react-native-loading-spinner-overlay";
import ValidationComponent from "react-native-form-validator";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";

import Interests from "./Interests";

const homePlace = {
  description: "Home",
  geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }
};
const workPlace = {
  description: "Work",
  geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }
};
import { connect } from "react-redux";
class Register extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: true,
      name: "",
      location: "",
      city: "",
      email: "",
      password: "",
      active: "true",
      textInput: []
    };
  }
  addTextInput = key => {
    let textInput = this.state.textInput;
    textInput.push(<TextInput key={key} />);
    this.setState({ textInput });
  };
  componentWillReceiveProps() {
    console.log(
      "this is the registerSuccess ### --------------------",
      this.props.registerSuccess
    );
    // if (this.props.registerSuccess == true) {
    //   Toast.show({
    //     text: "Required Field's !",
    //     buttonText: "Okay",
    //     buttonTextStyle: { color: "#FF0000" },
    //     buttonStyle: { backgroundColor: "#FFF" },
    //     textStyle: { color: "white" },
    //     style: { backgroundColor: "red" },
    //     duration: 1000
    //   });
    //   this.props.navigation.navigate("Interests");
    // }
  }

  register = () => {
    this.validate({
      name: { required: true },
      email: { email: true, password: true },
      password: { required: true }
    });
    if (this.state.name && this.state.email && this.state.password) {
      var userJson = {
        name: this.state.name ? this.state.name : "",
        location: this.state.location ? this.state.location : "",
        city: this.state.city ? this.state.city : "",
        email: this.state.email ? this.state.email : "",
        password: this.state.password ? this.state.password : ""
      };
      var { dispatch } = this.props;
      dispatch(register_user(userJson));
    } else {
      Toast.show({
        text: "Required Field's !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FF0000" },
        buttonStyle: { backgroundColor: "#FFF" },
        textStyle: { color: "white" },
        style: { backgroundColor: "red" },
        duration: 3000
      });
    }
  };

  componentDidUpdate() {
    console.log(
      "this is the registerSuccess *** --------------------",
      this.props.registerSuccess
    );
    if (this.props.registerSuccess == true) {
      Toast.show({
        text: "Required Field's !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FF0000" },
        buttonStyle: { backgroundColor: "#FFF" },
        textStyle: { color: "white" },
        style: { backgroundColor: "red" },
        duration: 500
      });

      this.props.navigation.navigate("Interests");
    }
  }
  render() {
    if (
      this.props.regLoading &&
      this.props.regLoading == true &&
      this.props.regLoading != false
    ) {
      return (
        <Spinner
          visible={this.props.regLoading}
          // textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
      );
      this.props.navigation.navigate("Interests");
    }
    //  else if (this.props.registerSuccess == true) {
    //   this.props.navigation.navigate("Interests");
    // }
    else {
      return (
        <ScrollView>
          <Container>
            {/* <Header noLeft style={styles.head}>
              <Left />
              <Body style={{ alignContent: "center" }}>
                <StatusBar backgroundColor="#cc0000" barStyle="light-content" />
                <Text style={{ fontSize: 20, color: "white" }}> Register </Text>
                <Image
                style={{ width: 80, height: 25 }}
                source={require("../../images/explora.png")}
              />
              </Body>
            </Header> */}
            {/* <Header style={styles.container}>
              <StatusBar backgroundColor='red' barStyle='light-content' />
              <Left style={{ marginLeft: 15 }}>
                <Image
                  style={styles.logo}
                  source={require('../../images/explora.png')}
                />
              </Left>
              <Body />
            </Header> */}
            <Container style={styles.regForm}>
              <TextInput
                style={styles.textimp}
                placeholder="Full Name"
                placeholderTextColor="darkgrey"
                onChangeText={name => this.setState({ name })}
                value={this.state.name}
              />
              {this.isFieldInError("name") &&
                this.getErrorsInField("name").map((errorMessage, index) => (
                  <Text key="index">{errorMessage}</Text>
                ))}

              {/* <GooglePlacesAutocomplete
                placeholder="Enter Location"
                minLength={2}
                autoFocus={false}
                returnKeyType={"default"}
                fetchDetails={true}
                styles={{
                  textInputContainer: {
                    backgroundColor: "rgba(0,0,0,0)",
                    borderTopWidth: 0,
                    borderBottomWidth: 0
                  },
                  textInput: {
                    marginLeft: 0,
                    marginRight: 0,
                    height: 38,
                    color: "#5d5d5d",
                    fontSize: 16
                  },
                  predefinedPlacesDescription: {
                    color: "#1faadb"
                  }
                }}
                currentLocation={false}
              /> */}

              <TextInput
                style={styles.textimp}
                placeholder="Location"
                placeholderTextColor="darkgrey"
                onChangeText={location => this.setState({ location })}
                value={this.state.location}
              />
              <TextInput
                style={styles.textimp}
                placeholder="Email"
                placeholderTextColor="darkgrey"
                onChangeText={email => this.setState({ email })}
                value={this.state.email}
              />
              {this.isFieldInError("email") &&
                this.getErrorsInField("email").map((errorMessage, index) => (
                  <Text key="index">{errorMessage}</Text>
                ))}
              <TextInput
                style={styles.textimp}
                placeholder="Password"
                placeholderTextColor="darkgrey"
                secureTextEntry={true}
                onChangeText={password => this.setState({ password })}
                value={this.state.password}
              />
              {this.isFieldInError("password") &&
                this.getErrorsInField("password").map((errorMessage, index) => (
                  <Text key="index">{errorMessage}</Text>
                ))}
              {/* <View style={styles.inline}>
                <Button rounded danger style={{ backgroundColor: "red" }}>
                  <Icon name="add" />
                </Button>
                <Text style={{ color: "red", marginTop: 7, marginLeft: 4 }}>
                  Add up to three images
                </Text>
              </View>

              <View>
                <Button
                  title="+"
                  onPress={() => this.addTextInput(this.state.textInput.length)}
                />
                {this.state.textInput.map((value, index) => {
                  return value;
                })}
              </View> */}
              <TouchableOpacity
                style={styles.button}
                onPress={this.register}
                disabled={this.props.userloginLoading}
                block
                light
              >
                <Text style={{ color: "black", fontSize: 20 }}>
                  Sign Up{" "}
                  <Icon
                    name="arrowright"
                    type="AntDesign"
                    style={{ color: "black", fontSize: 20 }}
                  />
                </Text>
              </TouchableOpacity>
            </Container>
            {/* <Fab
              direction="up"
              containerStyle={{}}
              style={{ backgroundColor: "#FF0000" }}
              position="bottomRight"
              onPress={this.register}
            >
              <Icon name="arrow-forward" />
            </Fab> */}
          </Container>
        </ScrollView>
      );
    }
  }
}

const styles = StyleSheet.create({
  head: {
    // height: 140,
    backgroundColor: "#cc0000"
  },
  container: {
    backgroundColor: "#FF0000"
  },
  spinnerTextStyle: {
    color: "#FF0000"
  },
  container1: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  logo: {
    height: 40,
    width: 120
  },
  textimp: {
    color: "grey",
    fontSize: 20,
    borderColor: "silver",
    borderWidth: 2,
    borderRadius: 25,
    padding: 13,
    marginTop: 15
  },
  textcity: {
    width: "80%",
    color: "red",
    fontSize: 20,
    borderColor: "red",
    borderWidth: 3,
    borderRadius: 8,
    padding: 10,
    marginTop: 15
  },
  regForm: {
    padding: 30,
    marginTop: 40
  },
  inline: {
    marginTop: 15,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  inlinecity: {
    marginTop: 2,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    backgroundColor: "#e1e1e1",
    borderWidth: 1,
    borderColor: "darkgrey",
    padding: 10,
    marginTop: 30,
    fontSize: 20
  }
});

const mapStateToProps = state => {
  return {
    regLoading: state.user.regLoading,
    registerSuccess: state.user.registerSuccess
  };
};

export default connect(mapStateToProps)(Register);

// ================ COMMENTED CODE COULD BE USED===============

{
  /* <View
            style={{
              borderRadius: 10,
              borderWidth: 3,
              borderColor: "#FF0000",
              marginTop: 9
            }}
          >
            <Picker
              selectedValue={this.state.location}
              style={{
                color: "red"
              }}
              mode="dialog"
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ location: itemValue })
              }
            >
              <Picker.Item label="Java" value="java" />
              <Picker.Item label="JavaScript" value="js" />
            </Picker>
          </View> */
}
