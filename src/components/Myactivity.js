import React, { Component } from "react";
import { StyleSheet, StatusBar, View, ScrollView } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import {
  Header,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Fab,
  Container,
  Picker,
  Item
} from "native-base";
import { getMyActivities } from "../actions";
import { connect } from "react-redux";
import { SearchBar } from "react-native-elements";
import Moment from "moment";
import FooterTabs from "./FooterTabs";
class Myactivity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myactivityData: [],
      activity: "",
      active: false,
      search: "",
      showSearch: false,
      localUserData: {}
    };
  }
  updateSearch = search => {
    this.setState({ search });
  };

  //   toggleStatus() {
  //     this.setState({
  //       showSearch: !this.state.showSearch
  //     });
  //     console.log("toggle button handler: " + this.state.showSearch);
  //   }
  _loadData = async () => {
    var data = await AsyncStorage.getItem("userData");
    this.setState({ localUserData: data });
    var id = JSON.parse(this.state.localUserData).userId;
    console.log("getting userid", id);
    this.props.dispatch(getMyActivities(id));
  };
  componentDidMount() {
    this._loadData();
    console.log("REDUCERSS", this.props.myactivityData);
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("REDUCERSS", this.props.myactivityData);
  }

  render() {
    // console.log("PROPS", this.props.myactivityData);
    return (
      <View style={{ flex: 1 }}>
        <Container>
          <Header noLeft style={styles.head}>
            <Left />
            <Body style={{ alignContent: "center" }}>
              <StatusBar backgroundColor="#cc0000" barStyle="light-content" />
              <Text style={{ fontSize: 20, color: "white" }}>
                My Activities{" "}
              </Text>
              {/* <Image
                style={{ width: 80, height: 25 }}
                source={require("../../images/explora.png")}
              /> */}
            </Body>
            <Right>
              <Button transparent>
                <Icon
                  style={{ color: "white" }}
                  onPress={() => this.toggleStatus()}
                  name="search"
                />
              </Button>
              <Button transparent>
                <View style={{}}>
                  <Icon
                    name="more"
                    style={{
                      zIndex: -999
                    }}
                  />
                  <Picker
                    style={{
                      zIndex: 999,
                      position: "absolute",
                      //right:0,
                      top: -35,
                      backgroundColor: "transparent"
                    }}
                    onValueChange={() => {}}
                    mode="dropdown"
                  >
                    <Item label="Wallet" value="key0" />
                    <Item label="ATM Card" value="key1" />
                  </Picker>
                </View>
              </Button>
            </Right>
          </Header>
          <SearchBar
            round={true}
            inputContainerStyle={{
              backgroundColor: "#FFFFFF",
              padding: 0,
              shadowColor: "#FFFFFF",
              borderColor: "#FFFFFF"
            }}
            containerStyle={{
              borderWidth: 0
            }}
            lightTheme={true}
            placeholder="Type Here..."
            onChangeText={this.updateSearch}
            value="search"
          />
          <ScrollView style="margin:30,paddingBottom:50;padding:20">
            {this.props.myactivityData.length > 0 &&
              this.props.myactivityData.map((act, index) => {
                return (
                  <Card key={index}>
                    <CardItem
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#efefef"
                      }}
                    >
                      <Left>
                        <Thumbnail source={require("../../images/ppl.png")} />
                        <Body>
                          <Text
                            style={{
                              color: "black",
                              textTransform: "capitalize"
                            }}
                          >
                            {act.activity}
                          </Text>
                          <View
                            style={{ flexDirection: "row", flexWrap: "wrap" }}
                          >
                            <Text style={{ color: "silver" }}>
                              <Icon
                                name="calendar"
                                type="AntDesign"
                                style={{
                                  color: "grey",
                                  fontSize: 18,
                                  paddingRight: 4
                                }}
                              />
                              {Moment(act.activityData).format(
                                "DD-MM-YY hh-mmA "
                              )}
                            </Text>
                            <Text note style={{ color: "silver" }}>
                              <Icon
                                name="location"
                                type="EvilIcons"
                                style={{
                                  color: "grey",
                                  fontSize: 18,
                                  paddingRight: 4
                                }}
                              />
                              {act.location}
                            </Text>
                          </View>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#efefef"
                      }}
                      cardBody
                    >
                      <View style={styles.msgReceivedAlignContainer}>
                        <Text style={{ paddingLeft: 20, color: "silver" }}>
                          <Icon
                            name="description"
                            type="MaterialIcons"
                            style={{ color: "grey", fontSize: 13 }}
                          />
                          {act.about}
                        </Text>
                      </View>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Button
                          style={styles.actBtn}
                          onPress={() =>
                            this.props.dispatch(
                              deleteActivity(
                                act._id,
                                JSON.parse(this.state.localUserData).userId
                              )
                            )
                          }
                        >
                          <Icon
                            style={{ color: "#cc0000" }}
                            active
                            name="close"
                            type="EvilIcons"
                          />
                          <Text style={{ color: "#cc0000" }}>Cancel </Text>
                        </Button>
                      </Left>
                      <Body />
                      <Right>
                        <Button
                          style={styles.actSBtn}
                          onPress={() =>
                            this.props.navigation.navigate("Activityr", {
                              aid: act._id
                            })
                          }
                        >
                          <Icon
                            active
                            style={{ color: "green" }}
                            name="eye"
                            type="AntDesign"
                          />
                          <Text style={{ color: "green" }}>View Request </Text>
                        </Button>
                      </Right>
                    </CardItem>
                  </Card>
                );
              })}
          </ScrollView>
        </Container>
        <FooterTabs navigation={this.props.navigation} />
        <Fab
          direction="up"
          // containerStyle={{}}
          containerStyle={{ bottom: 112, right: 30 }}
          style={{
            backgroundColor: "#323232"
          }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("AddActivity")}
        >
          <Icon name="add" />
        </Fab>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  head: {
    // height: 140,
    backgroundColor: "#cc0000"
  },
  mainContainer: {
    padding: 30,
    marginBottom: 30,
    paddingBottom: 90
  },
  msgReceivedAlignContainer: {
    flex: 1,
    flexDirection: "column",
    padding: 10
  },
  actBtn: {
    backgroundColor: "#FFFFFF"
  },
  actSBtn: {
    backgroundColor: "#FFFFFF"
  }
});

const mapStateToProps = state => {
  return {
    myactivityData: state.activity.myactivityData
  };
};
export default connect(mapStateToProps)(Myactivity);
