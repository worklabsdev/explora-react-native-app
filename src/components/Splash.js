import React, { Component } from "react";
import { AppRegistry, View, StyleSheet, Image } from "react-native";
import { AsyncStorage } from "@react-native-community/async-storage";

export default class Splash extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this._loadData();
  }
  _loadData = async () => {
    try {
      var data = await AsyncStorage.getItem("userData");
      if (data != null) {
        var tt =
          JSON.parse(data).isLoggedIn && JSON.parse(data).isLoggedIn == "true"
            ? true
            : false;
        // Activities: {
        tt == true
          ? this.props.navigation.navitgate("Activities")
          : this.props.navigation.navitgate("Login");
        // }
      } else {
        this.props.navigation.navitgate("Login");
      }
    } catch (error) {
      console.log("ERROR", error);
    }

    // console.log("ASYNC DATA", data);
  };

  render() {
    return (
      <View style={style.container}>
        <Image
          style={style.logo}
          source={require("../../images/explora.png")}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: "#cc0000",
    width: 450
  },
  logo: {
    height: 50,
    width: 150,
    marginLeft: 130,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 300
  }
});

AppRegistry.registerComponent("Splash", () => Splash);
