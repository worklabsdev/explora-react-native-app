import React, { Component } from "react";
import { connect } from "react-redux";
import { ScrollView, Text } from "react-native";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Left,
  Right,
  Body,
  Icon,
  Switch
} from "native-base";
import { getsingleactivity } from "../actions";

class Activityr extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activityId: ""
    };
  }
  componentDidMount() {
    const itemId = this.props.navigation.state.params.aid;
    console.log("ITEM ID", itemId);

    this.props.dispatch(getsingleactivity(itemId));
    // console.log("APPLICANTS", this.props.singleactivityData);
    // console.log("APPLICANTS", JSON.parse(this.props.singleactivityData.data));
  }
  //   componentDidUpdate() {
  //     console.log("APPLICANTS", JSON.parse(this.props.singleactivityData.data));
  //   }
  render() {
    console.log("APPLICANTS", this.props.singleactivityData.applicants);
    // {
    //   this.props.singleactivityData.applicants.map(item => {
    //     console.log("item", item);
    //   });
    // }
    return (
      <ScrollView>
        <List>
          <ListItem>
            <Left>
              <Thumbnail source={require("../../images/ppl.png")} />
              <Text>Simon Mignolet</Text>
            </Left>
            <Body />
            <Right>
              <Switch value={false} />
            </Right>
          </ListItem>
        </List>

        <List>
          {/* {Object.keys(this.props.singleactivityData.applicants).map(user => {
            console.log("INSIDE", user);
            return (
              <ListItem key={user} value={user}>
                <Left>
                  <Thumbnail source={require("../../images/ppl.png")} />
                </Left>
                <Body />

                <Text>{this.props.singleactivityData.applicants[user]}</Text>
                <Icon name="arrowright" type="AntDesign" />
              </ListItem>
            );
          })} */}
        </List>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    singleactivityData: state.activity.singleactivityData,
    getsingleactivityLoading: state.activity.getsingleactivityLoading
  };
};

export default connect(mapStateToProps)(Activityr);
