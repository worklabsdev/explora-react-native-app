import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { Container, Toast, Content, Text, Icon } from "native-base";
import { login_user } from "../actions";

import { connect } from "react-redux";
import ValidationComponent from "react-native-form-validator";
import interest from "../reducers/interestReducer";
class Login extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: true,
      email: "sameer.it78@gmail.com",
      password: "12345",

      // email: "mannu@yopmail.com",
      // password: "123456789",
      showToast: false
    };

    // this.login(); //remove
  }
  handleChange = fieldName => text => {
    this.setState({ [fieldName]: text });
  };

  login = () => {
    this.validate({
      email: { email: true, required: true },
      password: { required: true }
    });
    if (this.state.email && this.state.password) {
      var email = this.state.email;
      var password = this.state.password;
      var userjson = {
        email,
        password
      };
      var { dispatch } = this.props;
      dispatch(login_user(userjson));
    } else {
      Toast.show({
        text: "Required Field's !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FFF" },
        buttonStyle: { backgroundColor: "#FF0000" },
        textStyle: { color: "red" },
        style: { backgroundColor: "white" },
        duration: 3000
      });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.loginData == "") {
      this.props.loginData = state.user.loginData;
    }
    console.log(
      "This is data------",
      JSON.parse(JSON.stringify(this.props.loginData))
    );
    if (
      this.props.userLoggedIn &&
      this.props.userLoggedIn == true &&
      JSON.parse(JSON.stringify(this.props.loginData)).status == 200
    ) {
      var dd = JSON.parse(JSON.stringify(this.props.loginData));
      console.log("DDDDDDDDDDDDDDDDDD", dd.response.userData.interests);
      if (dd.response.userData.name) {
        var localData = {
          isLoggedIn: true,
          userId: dd.response.userData.id,
          name: dd.response.userData.name,
          email: dd.response.userData.email,
          location: dd.response.userData.location,
          interests: dd.response.userData.interests
        };
        this._loadData(localData);
      }

      this.props.navigation.navigate("Activities");
    }
    if (
      this.props.userLoggedIn == false &&
      JSON.parse(JSON.stringify(this.props.loginData)) &&
      JSON.parse(JSON.stringify(this.props.loginData)).status >= 400
    ) {
      Toast.show({
        text: "Invalid Credentials !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FFF" },
        buttonStyle: { backgroundColor: "#FF0000" },
        textStyle: { color: "red" },
        style: { backgroundColor: "white" },
        duration: 3000
      });
      console.log("INVALID CREDS");
      // Alert.alert("INVALID CREDS");
    }
  }
  _loadData = async data => {
    await AsyncStorage.setItem("userData", JSON.stringify(data))
      .then(() => {
        console.log("it is saved successfully");
      })
      .catch(() => {
        console.log("there was an error saving the product");
      });
  };

  handlePress = () => {
    this.setState({ active: !this.state.active });
  };

  render() {
    const uri = "../../images/explora.png";
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor="#cc0000" barStyle="light-content" />
        <Image
          style={styles.logo}
          source={require("../../images/explora.png")}
        />
        <Content style={styles.content}>
          <TextInput
            style={{
              color: "white",
              fontSize: 19,
              borderBottomWidth: 1,
              borderBottomColor: "#FFF"
            }}
            placeholder="email"
            placeholderTextColor="#FFF"
            autoCapitalize="none"
            onChangeText={email => this.setState({ email })}
            value={this.state.email}
            ref="email"
          />
          {this.isFieldInError("email") &&
            this.getErrorsInField("email").map((errorMessage, index) => (
              <Text key="index">{errorMessage}</Text>
            ))}

          <TextInput
            style={{
              color: "white",
              fontSize: 19,
              borderBottomWidth: 1,
              borderBottomColor: "#FFF",
              paddingTop: 25
            }}
            placeholder="password"
            placeholderTextColor="#FFF"
            value={this.state.password}
            autoCapitalize="none"
            onChangeText={password => this.setState({ password })}
            secureTextEntry={true}
            ref="password"
          />
          {this.isFieldInError("password") &&
            this.getErrorsInField("password").map((errorMessage, index) => (
              <Text key="index">{errorMessage}</Text>
            ))}
          <Text style={{ color: "white", marginLeft: 10, marginTop: 5 }}>
            forgot your password ?
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={this.login}
            disabled={this.props.userloginLoading}
            block
            light
          >
            <Text style={{ color: "white", fontSize: 20 }}>
              Sign In{" "}
              <Icon
                name="arrowright"
                type="AntDesign"
                style={{ color: "white", fontSize: 20 }}
              />
            </Text>
          </TouchableOpacity>
          <Text style={{ textAlign: "center", color: "white", marginTop: 40 }}>
            Don't have and account?{" "}
            {/* <Text onPress={() => this.props.navigation.navigate("Register")}>
              Sign Up
            </Text> */}
          </Text>
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.props.navigation.navigate("Register")}
            disabled={this.props.userloginLoading}
            block
            light
          >
            <Text style={{ color: "white", fontSize: 20 }}>
              Sign Up{" "}
              <Icon
                name="arrowright"
                type="AntDesign"
                style={{ color: "white", fontSize: 20 }}
              />
            </Text>
          </TouchableOpacity>
          {/* </Form> */}
        </Content>
        {/* <Fab
            direction="up"
            containerStyle={{}}
            style={{ backgroundColor: "#FFF" }}
            position="bottomRight"
            onPress={() => this.props.navigation.navigate("Register")}
          >
            <Icon style={{ color: "red" }} name="arrow-forward" />
          </Fab> */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#cc0000",
    alignItems: "center",
    padding: 30
  },
  container1: {
    alignSelf: "stretch",
    backgroundColor: "#FF0000"
  },
  content: {
    flex: 1,
    width: "90%",
    marginTop: 30
  },
  regForm: {
    backgroundColor: "#FF0000",
    flex: 1
  },
  logo: {
    height: 50,
    width: 150,
    marginTop: 150
  },
  header: {
    fontSize: 24,
    color: "black",
    padding: 10,
    paddingBottom: 10,
    marginBottom: 40,
    borderBottomColor: "red",
    borderBottomWidth: 1
  },
  btn: {
    marginTop: 20,
    padding: 7,
    color: "#FF0000"
  },
  btntext: {
    color: "red",
    fontWeight: "bold",
    fontSize: 15
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    backgroundColor: "#cc0000",
    borderWidth: 2,
    borderColor: "#fff",
    padding: 10,
    marginTop: 30,
    fontSize: 20
  }
});

const mapStateToProps = state => {
  console.log("STATE MAPSTATETOPROP", state);
  return {
    userloginLoading: state.user.userloginLoading,
    userLoggedIn: state.user.userLoggedIn,
    loginData: state.user.loginData
  };
};

export default connect(mapStateToProps)(Login);
