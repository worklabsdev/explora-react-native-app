import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  View,
  StatusBar,
  TextInput,
  ScrollView,
  TouchableOpacity
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Badge,
  Right,
  Fab,
  Icon,
  Text,
  Toast,
  Content,
  List,
  ListItem
} from "native-base";
import AsyncStorage from "@react-native-community/async-storage";
import { register_user, editProfile } from "../actions";
import Spinner from "react-native-loading-spinner-overlay";
import ValidationComponent from "react-native-form-validator";
import FooterTabs from "./FooterTabs";

export default class Editpi extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      location: ""
    };
  }

  componentDidMount() {
    this._loadData();
  }
  _loadData = async () => {
    var data = await AsyncStorage.getItem("userData");
    // console.log("ASYNC DATA", data);
    this.setState({ localUserData: data });
    var dat = JSON.parse(this.state.localUserData);
    console.log("ASYNC STORAGE", dat.interests);
    this.setState({
      email: dat.email,
      name: dat.name,
      location: dat.location
    });
  };
  render() {
    const { name, bio, location, email } = this.state;
    return (
      <ScrollView>
        <Container style={styles.regForm}>
          <TextInput
            style={styles.textimp}
            placeholder="Name"
            name="name"
            value={name}
            onChangeText={name => this.setState({ name })}
            placeholderTextColor="grey"
          />
          {/* <TextInput
                style={styles.textimp}
                placeholder="Bio"
                name="bio"
                value={bio}
                onChangeText={bio => this.setState({ bio })}
                placeholderTextColor="grey"
              /> */}
          <TextInput
            style={styles.textimp}
            placeholder="Location"
            name="location"
            value={location}
            onChangeText={location => this.setState({ location })}
            placeholderTextColor="grey"
          />
          <TextInput
            style={styles.textimp}
            placeholder="Email"
            name="email"
            value={email}
            onChangeText={email => this.setState({ email })}
            placeholderTextColor="grey"
          />
          <TouchableOpacity
            style={styles.button}
            block
            light
            onPress={this.updateUserProfile}
          >
            <Text style={{ color: "black", fontSize: 25 }}>Update</Text>
          </TouchableOpacity>
        </Container>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FF0000"
  },
  head: {
    // height: 140,
    backgroundColor: "#cc0000"
  },
  textimp: {
    color: "grey",
    fontSize: 20,
    borderColor: "#efefef",
    borderWidth: 2,
    borderRadius: 25,
    padding: 13,
    marginTop: 15
  },
  spinnerTextStyle: {
    color: "#FF0000"
  },
  container1: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  logo: {
    height: 40,
    width: 120
  },
  // button: {
  //   borderRadius: 25,
  //   alignItems: "center",
  //   backgroundColor: "#fff",
  //   borderWidth: 2,
  //   borderColor: "#cc0000",
  //   padding: 10,
  //   marginTop: 30,
  //   fontSize: 20
  // },
  button: {
    borderRadius: 25,
    alignItems: "center",
    backgroundColor: "#efefef",
    borderWidth: 1,
    borderColor: "darkgrey",
    padding: 10,
    marginTop: 30,
    fontSize: 20
  },
  // textimp: {
  //   color: "red",
  //   fontSize: 20,
  //   borderColor: "red",
  //   borderWidth: 3,
  //   borderRadius: 8,
  //   padding: 10,
  //   marginTop: 15
  // },
  textcity: {
    width: "80%",
    color: "red",
    fontSize: 20,
    borderColor: "red",
    borderWidth: 3,
    borderRadius: 8,
    padding: 10,
    marginTop: 15
  },
  regForm: {
    padding: 30,
    marginTop: 40
  },
  inline: {
    marginTop: 15,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  inlinecity: {
    marginTop: 2,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  }
});
