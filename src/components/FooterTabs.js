import React, { Component } from "react";
import { Text } from "react-native";
import { Footer, FooterTab, Button, Icon, Badge } from "native-base";
// import console = require('console');

const FooterTabs = props => {
  console.log("propsss", props);
  // var isActive = this.context.router.route.location.pathname === this.props.to;
  // console.log("CURRENT PATH", isActive);
  // var className = isActive ? "active" : "";
  return (
    <Footer
      style={{
        backgroundColor: "#FFFFFF",
        borderTopColor: "#FFFFFF",
        borderBottomColor: "#FFFFFF"
      }}
    >
      <FooterTab
        style={{
          backgroundColor: "#FFFFFF",
          borderTopColor: "#FFFFFF",
          borderBottomColor: "#FFFFFF"
        }}
      >
        <Button onPress={() => props.navigation.navigate("Profile")}>
          <Icon name="user" type="Feather" style={{ color: "grey" }} />
          <Text style={{ color: "grey", fontSize: 9 }}>PROFILE</Text>
        </Button>
        <Button onPress={() => props.navigation.navigate("Activities")}>
          <Icon name="users" type="Feather" style={{ color: "grey" }} />
          <Text style={{ color: "grey", fontSize: 9 }}>FEED</Text>
        </Button>
        <Button
          style={{ fontSize: 12 }}
          onPress={() => props.navigation.navigate("Myactivity")}
        >
          <Icon name="home" type="Feather" style={{ color: "grey" }} />
          <Text style={{ color: "grey", fontSize: 9 }}>HOME</Text>
        </Button>
        <Button
          style={{ fontSize: 12 }}
          onPress={() => props.navigation.navigate("Myrequests")}
        >
          <Icon name="eye" type="Feather" style={{ color: "grey" }} />
          <Text style={{ color: "grey", fontSize: 9 }}>REQUEST</Text>
        </Button>
        <Button badge vertical style={{ fontSize: 12 }}>
          <Badge>
            <Text style={{ color: "white" }}>51</Text>
          </Badge>
          <Icon name="bell" type="Entypo" style={{ color: "grey" }} />
          <Text style={{ color: "grey", fontSize: 9 }}>NOTIFICATION</Text>
        </Button>
      </FooterTab>
    </Footer>
  );
};

export default FooterTabs;
