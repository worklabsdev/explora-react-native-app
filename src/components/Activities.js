import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  StatusBar,
  View,
  ScrollView,
  TextInput
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import {
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Toast,
  Fab,
  Title,
  Subtitle,
  Container,
  Input,
  Picker,
  Item
} from "native-base";
import { list_activities, deleteActivity, applyActivity } from "../actions";
import { connect } from "react-redux";
import { SearchBar } from "react-native-elements";
import Moment from "moment";
import FooterTabs from "./FooterTabs";
import {
  Menu,
  MenuProvider,
  MenuOptions,
  MenuOption,
  MenuTrigger
} from "react-native-popup-menu";
class Activities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activitiesData: [],
      activity: "",
      active: false,
      search: "",
      showSearch: false,
      localUserData: {}
    };
  }
  updateSearch = search => {
    this.setState({ search });
  };

  toggleStatus() {
    this.setState({
      showSearch: !this.state.showSearch
    });
    console.log("toggle button handler: " + this.state.showSearch);
  }
  _loadData = async () => {
    var data = await AsyncStorage.getItem("userData");
    // console.log("ASYNC DATA", data);
    this.setState({ localUserData: data });
    var tt = JSON.parse(this.state.localUserData).userId;
    this.props.dispatch(list_activities(tt));
  };
  componentDidMount() {
    // didFocus
    // this.props.navigation.addListner("didFocus", () => {
    this._loadData();
    // alert(JSON.parse(JSON.stringify(tt)));
    // var id = JSON.parse(this.state.localUserData);
    // console.log("IDIDIDIDIDIDI", dd);

    // });
    // this.props.dispatch(list_activities());
    // console.log("PROPSSSSSSSSSS", this.props.activitiesData);
  }

  componentDidUpdate(prevProps, prevState) {
    // console.log(
    //   "you are getting data UPDATE UPDATE-------",
    //   JSON.parse(this.props.applySuccessData).status
    // );
    console.log("APPLY DATA", this.props.applySuccessData);
    if (
      this.props.applySuccessData &&
      this.props.applySuccessData.status == 200
    ) {
      Toast.show({
        text: "Request posted !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FFF" },
        buttonStyle: { backgroundColor: "#FF0000" },
        textStyle: { color: "red" },
        style: { backgroundColor: "white" },
        duration: 3000
      });

      // this._loadData();
    }
    // this._loadData();
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (
  //     this.state.activity.activitiesData.length !==
  //     nextState.activity.activitiesData.length
  //   ) {
  //     return true;
  //   }
  //   return false;
  // }

  render() {
    // const { search } = this.state;

    console.log("+++++++++++++++++++++", this.props.activitiesData);
    return (
      <View style={{ flex: 1 }}>
        <Container>
          {/* <Header noLeft style={styles.head}>
            <Left />
            <Body style={{ alignContent: "center" }}>
              <StatusBar backgroundColor="#cc0000" barStyle="light-content" />
              <Text style={{ fontSize: 20, color: "white" }}> Activities </Text>
            </Body>
            <Right>
              <Button transparent>
                <Icon
                  style={{ color: "white" }}
                  onPress={() => this.toggleStatus()}
                  name="search"
                />
              </Button>
              <Button transparent>
                <View style={{}}>
                  <Icon
                    name="more"
                    style={{
                      zIndex: -999
                    }}
                  />
                  <Picker
                    style={{
                      zIndex: 999,
                      position: "absolute",
                      //right:0,
                      top: -35,
                      backgroundColor: "transparent"
                    }}
                    onValueChange={() => {}}
                    mode="dropdown"
                  >
                    <Item label="Wallet" value="key0" />
                    <Item label="ATM Card" value="key1" />
                  </Picker>
                </View>
              </Button>
            </Right>
          </Header> */}

          <SearchBar
            round={true}
            inputContainerStyle={{
              backgroundColor: "#FFFFFF",
              padding: 0,
              shadowColor: "#FFFFFF",
              borderColor: "#FFFFFF"
            }}
            containerStyle={{
              borderWidth: 0
              // backgroundColor: "#FFFFFF",
              // borderColor: "#FFFFFF",
              // padding: 0,
              // shadowColor: "#FFFFFF"
            }}
            lightTheme={true}
            placeholder="Type Here..."
            onChangeText={this.updateSearch}
            value="search"
          />

          {/* <Content style={styles.mainContainer}> */}
          <ScrollView style="margin:30,paddingBottom:50;padding:20">
            {this.props.activitiesData.length > 0 &&
              this.props.activitiesData.map((act, index) => {
                return (
                  <Card key={index}>
                    <CardItem
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#efefef"
                      }}
                    >
                      <Left>
                        <Thumbnail source={require("../../images/ppl.png")} />
                        <Body>
                          <Text
                            style={{
                              color: "black",
                              textTransform: "capitalize"
                            }}
                          >
                            {act.activity}
                          </Text>
                          <View
                            style={{ flexDirection: "row", flexWrap: "wrap" }}
                          >
                            <Text style={{ color: "silver" }}>
                              <Icon
                                name="calendar"
                                type="AntDesign"
                                style={{
                                  color: "grey",
                                  fontSize: 18,
                                  paddingRight: 4
                                }}
                              />
                              {Moment(act.activityData).format(
                                "DD-MM-YY hh-mmA "
                              )}
                            </Text>
                            <Text note style={{ color: "silver" }}>
                              <Icon
                                name="location"
                                type="EvilIcons"
                                style={{
                                  color: "grey",
                                  fontSize: 18,
                                  paddingRight: 4
                                }}
                              />
                              {act.location}
                            </Text>
                          </View>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#efefef"
                      }}
                      cardBody
                    >
                      <View style={styles.msgReceivedAlignContainer}>
                        {/* <Text style={{ paddingLeft: 20, color: "grey" }}>
                          <Icon
                            name="calendar"
                            type="AntDesign"
                            style={{ color: "grey", fontSize: 8 }}
                          />
                          {Moment(act.activityData).format("DD-MM-YY hh-mmA ")}
                        </Text> */}

                        <Text style={{ paddingLeft: 20, color: "silver" }}>
                          <Icon
                            name="description"
                            type="MaterialIcons"
                            style={{ color: "grey", fontSize: 13 }}
                          />
                          {act.about}
                        </Text>
                      </View>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Button
                          style={styles.actBtn}
                          onPress={() =>
                            this.props.dispatch(
                              deleteActivity(
                                act._id,
                                JSON.parse(this.state.localUserData).userId
                              )
                            )
                          }
                        >
                          <Icon
                            style={{ color: "#cc0000" }}
                            active
                            name="close"
                            type="EvilIcons"
                          />
                          <Text style={{ color: "#cc0000" }}>Cancel </Text>
                        </Button>
                        {/* <Button
                          rounded
                          danger
                          onPress={() =>
                            this.props.dispatch(deleteActivity(act._id))
                          }
                        >
                          <Icon active name="md-close" />
                        </Button> */}
                      </Left>
                      <Body />
                      <Right>
                        <Button
                          style={styles.actSBtn}
                          onPress={() => {
                            this.props.dispatch(
                              applyActivity(
                                act._id,
                                JSON.parse(this.state.localUserData).userId
                              )
                            );
                          }}
                        >
                          <Icon
                            active
                            style={{ color: "green" }}
                            name="check"
                            type="AntDesign"
                          />
                          <Text style={{ color: "green" }}>Apply </Text>
                        </Button>
                      </Right>
                    </CardItem>
                  </Card>
                );
              })}
          </ScrollView>
        </Container>
        {/* </ScrollView> */}
        <FooterTabs navigation={this.props.navigation} />
        {/* <Fab
          direction="up"
          containerStyle={{ bottom: 112, right: 30 }}
          style={{
            backgroundColor: "#323232"
          }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("AddActivity")}
        >
          <Icon name="add" />
        </Fab> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  head: {
    // height: 140,
    backgroundColor: "#cc0000"
  },
  mainContainer: {
    padding: 30,
    marginBottom: 30,
    paddingBottom: 90
  },
  msgReceivedAlignContainer: {
    flex: 1,
    flexDirection: "column",
    padding: 10
  },
  actBtn: {
    backgroundColor: "#FFFFFF"
  },
  actSBtn: {
    backgroundColor: "#FFFFFF"
  }
});

const mapStateToProps = state => {
  return {
    activitiesData: state.activity.activitiesData,
    applyactivityLoading: state.activity.applyactivityLoading,
    applySuccessData: state.activity.applySuccessData
  };
};
export default connect(mapStateToProps)(Activities);
