import React, { Component } from "react";
import { StyleSheet, StatusBar, View, ScrollView } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import {
  Header,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Icon,
  Button,
  Left,
  Body,
  Right,
  Fab,
  Container,
  Picker,
  Item,
  Badge
} from "native-base";
import { getMyRequests } from "../actions";
import { connect } from "react-redux";
import { SearchBar } from "react-native-elements";
import Moment from "moment";
import FooterTabs from "./FooterTabs";
class Myrequests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      myactivityData: [],
      activity: "",
      active: false,
      search: "",
      showSearch: false,
      localUserData: {}
    };
  }
  _loadData = async () => {
    var data = await AsyncStorage.getItem("userData");
    this.setState({ localUserData: data });
    var id = JSON.parse(this.state.localUserData).userId;
    console.log("getting userid", id);
    this.props.dispatch(getMyRequests(id));
  };
  componentDidMount() {
    this._loadData();
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("REDUCERSS", this.props.myactivityData);
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Container>
          <SearchBar
            round={true}
            inputContainerStyle={{
              backgroundColor: "#FFFFFF",
              padding: 0,
              shadowColor: "#FFFFFF",
              borderColor: "#FFFFFF"
            }}
            containerStyle={{
              borderWidth: 0
            }}
            lightTheme={true}
            placeholder="Type Here..."
            onChangeText={this.updateSearch}
            value="search"
          />
          <ScrollView style="margin:30,paddingBottom:50;padding:20">
            {this.props.myrequestsData.length > 0 &&
              this.props.myrequestsData.map((act, index) => {
                return (
                  <Card key={index}>
                    <CardItem
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#efefef"
                      }}
                    >
                      <Left>
                        <Thumbnail source={require("../../images/ppl.png")} />
                        <Body>
                          <Text
                            style={{
                              color: "black",
                              textTransform: "capitalize"
                            }}
                          >
                            {act.activity}
                          </Text>

                          <View
                            style={{ flexDirection: "row", flexWrap: "wrap" }}
                          >
                            <Icon
                              name="calendar"
                              type="AntDesign"
                              style={{
                                color: "grey",
                                fontSize: 18,
                                paddingRight: 7
                              }}
                            />
                            <Text style={{ color: "silver" }}>
                              {Moment(act.activityData).format(
                                "DD-MM-YY hh-mmA "
                              )}
                            </Text>
                            <Icon
                              name="location"
                              type="EvilIcons"
                              style={{
                                color: "grey",
                                fontSize: 18,
                                paddingRight: 4
                              }}
                            />
                            <Text
                              note
                              style={{ color: "silver", paddingRight: 30 }}
                            >
                              {act.location}
                            </Text>
                            <Text note style={{ color: "orange" }}>
                              <Icon
                                name="dot-circle"
                                type="FontAwesome5"
                                style={{
                                  color: "orange",
                                  fontSize: 18,
                                  paddingRight: 4
                                }}
                              />{" "}
                              Pending
                            </Text>
                          </View>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem
                      style={{
                        borderBottomWidth: 1,
                        borderBottomColor: "#efefef"
                      }}
                      cardBody
                    >
                      <View style={styles.msgReceivedAlignContainer}>
                        <Text style={{ paddingLeft: 20, color: "silver" }}>
                          <Icon
                            name="description"
                            type="MaterialIcons"
                            style={{ color: "grey", fontSize: 13 }}
                          />
                          {act.about}
                        </Text>
                      </View>
                    </CardItem>
                    <CardItem>
                      {/* <Left>
                        <Button
                          style={styles.actBtn}
                          onPress={() =>
                            this.props.dispatch(
                              deleteActivity(
                                act._id,
                                JSON.parse(this.state.localUserData).userId
                              )
                            )
                          }
                        >
                          <Icon
                            style={{ color: "#cc0000" }}
                            active
                            name="close"
                            type="EvilIcons"
                          />
                          <Text style={{ color: "#cc0000" }}>Cancel </Text>
                        </Button>
                      </Left>
                      <Body />
                      <Right>
                        <Button
                          style={styles.actSBtn}
                          onPress={() =>
                            this.props.navigation.navigate("Activityr", {
                              aid: act._id
                            })
                          }
                        >
                          <Icon
                            active
                            style={{ color: "green" }}
                            name="eye"
                            type="AntDesign"
                          />
                          <Text style={{ color: "green" }}>View Request </Text>
                        </Button>
                      </Right> */}
                    </CardItem>
                  </Card>
                );
              })}
          </ScrollView>
        </Container>
        <FooterTabs navigation={this.props.navigation} />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  head: {
    // height: 140,
    backgroundColor: "#cc0000"
  },
  mainContainer: {
    padding: 30,
    marginBottom: 30,
    paddingBottom: 90
  },
  msgReceivedAlignContainer: {
    flex: 1,
    flexDirection: "column",
    padding: 10
  },
  actBtn: {
    backgroundColor: "#FFFFFF"
  },
  actSBtn: {
    backgroundColor: "#FFFFFF"
  }
});

const mapStateToProps = state => {
  return {
    myrequestsData: state.activity.myrequestsData,
    getmyrequestsLoading: state.activity.getmyrequestsLoading
  };
};

export default connect(mapStateToProps)(Myrequests);
