import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  View,
  StatusBar,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Alert
} from "react-native";
import {
  Container,
  Fab,
  Text,
  Toast,
  Textarea,
  DatePicker,
  Picker
} from "native-base";
import { save_activity } from "../actions";
import Spinner from "react-native-loading-spinner-overlay";
import AsyncStorage from "@react-native-community/async-storage";
import ValidationComponent from "react-native-form-validator";
import activity from "../reducers/activityReducer";
import { connect } from "react-redux";
import FooterTabs from "./FooterTabs";

class Addactivity extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      activity: "",
      activityDate: "",
      location: "",
      about: "",
      selected: "",
      userId: ""
    };
  }
  componentDidMount() {
    this._loadData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.savedActivity &&
      this.props.savedActivity == true &&
      this.props.activitiesData.status == 200
    ) {
      Toast.show({
        text: "Saved Successfully !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FFF" },
        buttonStyle: { backgroundColor: "#FF0000" },
        textStyle: { color: "red" },
        style: { backgroundColor: "white" },
        duration: 3000
      });
      this.props.navigation.navigate("Myactivity");
    }
  }
  _loadData = async () => {
    var data = await AsyncStorage.getItem("userData");
    this.setState({ localUserData: data });
    var tt = JSON.parse(this.state.localUserData).userId;
    this.setState({ userId: tt });
  };
  saveactivity = () => {
    this.validate({
      activity: { required: true },
      activityDate: { required: true },
      location: { required: true },
      about: { required: true }
    });
    if (
      this.state.activity &&
      this.state.activityDate &&
      this.state.location &&
      this.state.about
    ) {
      console.log("SAVE ACTIVITY", this.state.userId);
      var userJson = {
        activity: this.state.activity ? this.state.activity : "",
        activityDate: this.state.activityDate ? this.state.activityDate : "",
        location: this.state.location ? this.state.location : "",
        about: this.state.about ? this.state.about : "",
        userId: this.state.userId ? this.state.userId : ""
      };
      var { dispatch } = this.props;
      dispatch(save_activity(userJson));
    } else {
      Alert.alert("Please fill required fields");
      // Toast.show({
      //   text: "Required Field's !",
      //   buttonText: "Okay",
      //   buttonTextStyle: { color: "#FFF" },
      //   buttonStyle: { backgroundColor: "#FF0000" },
      //   textStyle: { color: "red" },
      //   style: { backgroundColor: "white" },
      //   duration: 3000
      // });
    }
  };
  render() {
    return (
      <>
        <ScrollView>
          <Container>
            <Container style={styles.activityForm}>
              <TextInput
                style={styles.textimp}
                placeholder="Activity"
                placeholderTextColor="grey"
                autoCapitalize="none"
                onChangeText={activity => this.setState({ activity })}
                value={this.state.activity}
              />
              {this.isFieldInError("activity") &&
                this.getErrorsInField("activity").map((errorMessage, index) => (
                  <Text key="index">{errorMessage}</Text>
                ))}
              <View
                style={{
                  borderColor: "#efefef",
                  borderWidth: 2,
                  marginTop: 15,
                  height: 51,
                  borderRadius: 25
                }}
              >
                <DatePicker
                  defaultDate={new Date(2018, 4, 4)}
                  minimumDate={new Date(2018, 1, 1)}
                  maximumDate={new Date(2018, 12, 31)}
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={true}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderText="dd/mm/yyyy"
                  textStyle={{ color: "grey" }}
                  placeHolderTextStyle={{ color: "grey" }}
                  onDateChange={activityDate => this.setState({ activityDate })}
                  disabled={false}
                />
              </View>
              <TextInput
                style={styles.textimp}
                placeholder="Location"
                placeholderTextColor="grey"
                autoCapitalize="none"
                onChangeText={location => this.setState({ location })}
                value={this.state.location}
              />
              {/* <Picker
                  mode="dropdown"
                  iosHeader="Select your SIM"
                  iosIcon={<Icon name="arrow-down" />}
                  color="#ff0000"
                  selectedValue={this.state.lookingFor}
                  onValueChange={(lookingFor, itemIndex) =>
                    this.setState({ lookingFor: lookingFor })
                  }
                  // value={this.state.lookingFor}
                >
                  <Picker.Item label="Looking for" value="" />
                  <Picker.Item label="Cinema" value="cinema" />
                  <Picker.Item label="Food" value="food" />
                  <Picker.Item label="Theater" value="theater" />
                </Picker> */}
              <Textarea
                rowSpan={5}
                style={styles.textimp}
                placeholder="about"
                autoCapitalize="none"
                onChangeText={about => this.setState({ about })}
                value={this.state.about}
                placeholderTextColor="grey"
              />
              <TouchableOpacity
                style={styles.button}
                onPress={this.saveactivity}
                // disabled={this.props.userloginLoading}
                block
                light
              >
                <Text style={{ color: "black", fontSize: 20 }}>Create</Text>
              </TouchableOpacity>
            </Container>
            {/* <Fab
              direction="up"
              containerStyle={{}}
              style={{ backgroundColor: "#FF0000", marginBottom: 40 }}
              position="bottomRight"
              onPress={this.saveactivity}
            >
              <Icon name="arrow-forward" />
            </Fab> */}
          </Container>
        </ScrollView>
        <FooterTabs navigation={this.props.navigation} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    height: 40,
    width: 120,
    marginLeft: 20
  },
  head: {
    backgroundColor: "#ff3333"
  },
  foot: {
    backgroundColor: "#fff"
  },
  button: {
    borderRadius: 25,
    alignItems: "center",
    backgroundColor: "#e1e1e1",
    borderWidth: 1,
    borderColor: "darkgrey",
    padding: 10,
    marginTop: 30,
    fontSize: 20
  },
  active: {
    color: "#ff3333"
    // backgroundColor: "#ff3333"
  },
  about: {
    borderColor: "#ff3333"
  },
  textimp: {
    color: "grey",
    fontSize: 20,
    borderColor: "#efefef",
    borderWidth: 2,
    borderRadius: 25,
    padding: 13,
    marginTop: 15
  },
  activityForm: {
    padding: 30,
    marginTop: 15
  }
});

const mapStateToProps = state => {
  return {
    saveActivityLoading: state.activity.saveActivityLoading,
    savedActivity: state.activity.savedActivity,
    activitiesData: state.activity.activitiesData
  };
};

export default connect(mapStateToProps)(Addactivity);
