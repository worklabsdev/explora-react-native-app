import React, { Component } from "react";
import { View, StyleSheet, Image, StatusBar } from "react-native";
import { Container, Icon, Fab, Text, Toast } from "native-base";
import { get_interest, editInterests } from "../actions";
import { connect } from "react-redux";
import { TagSelect } from "react-native-tag-select";
import AsyncStorage from "@react-native-community/async-storage";
import FooterTabs from "./FooterTabs";

class Interests extends Component {
  constructor(props) {
    super(props);
    this.state = {
      interests: [],
      counter: 3,
      getInterestLoading: "",
      error: false
    };
    // this.tag = React.createRef();
  }
  componentDidMount() {
    this.props.dispatch(get_interest());
    this.setState({
      interests: this.props.interestData.map(interest => ({
        id: interest._id,
        label: interest.interestName
      }))
    });
  }
  componentDidUpdate(prevProps, prevState) {
    console.log("COMPONENT DID UPDATE", this.props.interestData);
    console.log("COMPONENT DID SUCCESS DATA", this.props.successData);
    if (this.props.successData && this.props.successData.status == 200) {
      var dd = JSON.parse(JSON.stringify(this.props.loginData));
      console.log("DDDDDDDDDDDDDDDDDD", dd.data.name);
      if (dd.data.name) {
        var localData = {
          isLoggedIn: true,
          userId: dd.data._id,
          name: dd.data.name,
          email: dd.data.email,
          location: dd.data.location
        };
        console.log("LOCALDATA", localData);
        // setTimeout(() => {
        this._loadData(localData);
        // }, 1500);

        Toast.show({
          text: "Saved Successfully !",
          buttonText: "Okay",
          buttonTextStyle: { color: "#FFF" },
          buttonStyle: { backgroundColor: "#FF0000" },
          textStyle: { color: "red" },
          style: { backgroundColor: "white" },
          duration: 3000
        });
        // this._gettingData();
        // var data = await AsyncStorage.getItem("userData");
        // console.log("ASYNC DATA", data);
        // this.props.savedActivity = false;
        this.props.navigation.navigate("Activities");
      }
    }
  }
  _loadData = async data => {
    await AsyncStorage.setItem("userData", JSON.stringify(data))
      .then(() => {
        console.log("it is saved successfully");
      })
      .catch(() => {
        console.log("there was an error saving the product");
      });
  };
  // _gettingData = async () => {
  //   var data = await AsyncStorage.getItem("userData");
  //   console.log("ASYNC DATA", data);
  //   this.setState({ localUserData: data });
  //   var tt = JSON.parse(this.state.localUserData).userId;
  //   this.props.dispatch(list_activities(tt));
  //   // console.log("TTTTTTTTTTTTTT", tt);
  //   // return tt;
  //   // console.log("ASYNC DATA", JSON.parse(this.state.localUserData));
  // };

  updateUserInterests = () => {
    console.log("gg", this.tag.itemsSelected);
    console.log("HH", this.props.loginData);
    if (this.tag.totalSelected >= 3) {
      this.setState({
        error: false
      });
      console.log("INTEREST IN COMPONENTS", this.tag.itemsSelected);
      console.log("ID IN COMPONENTS", this.props.loginData.data._id);

      this.props.dispatch(
        editInterests(this.tag.itemsSelected, this.props.loginData.data._id)
      );
    } else {
      this.setState({ error: true });
    }
  };

  render() {
    console.log(this.state);
    return (
      <>
        <Container>
          {/* <Header style={styles.head}>
            <Left>
              <Button
                onPress={() => this.props.navigation.navigate("Activities")}
                transparent
              >
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <StatusBar backgroundColor="red" barStyle="light-content" />
              <Image
                style={{ width: 80, height: 25 }}
                source={require("../../images/explora.png")}
              />
            </Body>
            <Right>
              <Button transparent>
                <Icon name="more" />
              </Button>
            </Right>
          </Header> */}
          {/* <Header style={{ backgroundColor: '#FF0000' }}>
            <StatusBar backgroundColor='red' barStyle='light-content' />
            <Left style={{ marginLeft: 15 }}>
              <Image
                style={styles.logo}
                source={require('../../images/explora.png')}
              />
            </Left>
            <Body />
          </Header> */}

          <View style={styles.container}>
            {/* <Text style={styles.labelText}>
            Please choose atleast three interest::
          </Text>
          <TagSelect
            data={this.props.interestData}
            max={3}
            ref={tag => {
              this.tag = tag;
            }}
            onMaxError={() => {
              Alert.alert("Ops", "Max reached");
            }}
          />
          <View style={styles.buttonContainer}>
            <View style={styles.buttonInner}>
              <Button
                title="Get selected count"
                style={styles.button}
                onPress={() => {
                  Alert.alert(
                    "Selected count",
                    `Total: ${this.tag.totalSelected}`
                  );
                }}
              />
            </View>
            <View>
              <Button
                title="Get selected"
                onPress={() => {
                  Alert.alert(
                    "Selected items:",
                    JSON.stringify(this.tag.itemsSelected)
                  );
                }}
              />
            </View>
          </View> */}
            <Text style={styles.labelText}>
              Please choose atleast three interest:
            </Text>
            {this.props.interestData.length > 0 && (
              <TagSelect
                data={this.props.interestData.map(interest => ({
                  id: interest._id,
                  label: interest.interestName
                }))}
                ref={tag => {
                  this.tag = tag;
                }}
                itemStyle={styles.item}
                itemLabelStyle={styles.label}
                itemStyleSelected={styles.itemSelected}
                itemLabelStyleSelected={styles.labelSelected}
              />
            )}
            {this.state.error && (
              <Text>Please select {3 - this.tag.totalSelected} more tags.</Text>
            )}

            <Fab
              direction="up"
              containerStyle={{}}
              style={{ backgroundColor: "#FF0000" }}
              position="bottomRight"
              // onPress={this.updateUserImterest}
              onPress={this.updateUserInterests}
            >
              <Icon name="arrow-forward" />
            </Fab>
          </View>
        </Container>
        <FooterTabs navigation={this.props.navigation} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    padding: 20
    // marginTop: 50,
    // marginLeft: 15
  },
  head: {
    // height: 140,
    backgroundColor: "#ff3333"
  },
  buttonContainer: {
    padding: 15
  },
  logo: {
    height: 40,
    width: 120
  },
  buttonInner: {
    marginBottom: 15
  },
  labelText: {
    color: "#FF0000",
    fontSize: 20,
    marginTop: 30,
    fontWeight: "700",
    marginBottom: 15
  },
  item: {
    borderWidth: 1,
    borderColor: "#FF0000",
    backgroundColor: "#FFF"
  },
  label: {
    color: "#FF0000"
  },
  itemSelected: {
    backgroundColor: "#FF0000"
  },
  labelSelected: {
    color: "#FFF"
  }
});

const mapStateToProps = state => {
  return {
    getInterestLoading: state.interest.getInterestLoading,
    interestData: state.interest.interestData,
    loginData: state.user.loginData,
    successData: state.interest.successData
  };
};
export default connect(mapStateToProps)(Interests);
