import React, { Component } from "react";

import {
  StyleSheet,
  Image,
  View,
  StatusBar,
  TouchableHighlight
} from "react-native";
import {
  Container,
  Header,
  Left,
  Body,
  Button,
  Badge,
  Right,
  Fab,
  Icon,
  Text,
  Toast,
  Content,
  List,
  ListItem
} from "native-base";
import Modal from "react-native-modal";
import AsyncStorage from "@react-native-community/async-storage";
import { register_user, editProfile, logoutClear } from "../actions";
import Spinner from "react-native-loading-spinner-overlay";
import ValidationComponent from "react-native-form-validator";
import FooterTabs from "./FooterTabs";

import { connect } from "react-redux";
class Profile extends ValidationComponent {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      name: "",
      location: "",
      bio: "",
      interests: [],
      isModalVisible: false
    };
  }
  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  handleChange = e => {
    console.log(e.target);
    this.setState({ [e.target.name]: e.target.value });
  };

  submitProfile = () => {
    const { name, bio, location } = this.state;
    const data = { name, bio, location };
    this.props.dispatch(
      editProfile(data, this.props.loginData.response.userData.id)
    );
  };

  updateUserProfile = () => {
    this.validate({
      name: { required: true },
      location: { required: true }
    });
    if (this.state.name && this.state.location) {
      var name = this.state.name;
      var location = this.state.location;
      var bio = this.state.bio;
      var userjson = {
        name,
        bio,
        location
      };
      console.log("IN PROFILE", userjson);
      var { dispatch } = this.props;
      dispatch(
        editProfile(userjson, this.props.loginData.response.userData.id)
      );
    } else {
      Toast.show({
        text: "Required Field's !",
        buttonText: "Okay",
        buttonTextStyle: { color: "#FFF" },
        buttonStyle: { backgroundColor: "#FF0000" },
        textStyle: { color: "red" },
        style: { backgroundColor: "white" },
        duration: 3000
      });
    }
  };

  componentDidMount() {
    this._loadData();
  }
  _loadData = async () => {
    var data = await AsyncStorage.getItem("userData");
    // console.log("ASYNC DATA", data);
    this.setState({ localUserData: data });
    var dat = JSON.parse(this.state.localUserData);

    console.log("ASYNC STORAGE", dat.interests);
    this.setState({
      email: dat.email,
      name: dat.name,
      location: dat.location,
      interests: dat.interests
    });
    // alert("SETSTATE");
    // this.state.interests.forEach(element => {
    //   this.state.interests.push(element);
    // });
    // alert("SETSTATE");
    console.log("STATE INTERSTS", this.state.interests);

    // console.log("FIRST OBJECT", this.state.interests[0]);
    // this.props.dispatch(list_activities(dat.userId));
  };
  clearAsyncStorage = async () => {
    var { dispatch } = this.props;
    this.props.userLoggedIn = false;
    this.props.loginData = {};

    await AsyncStorage.clear();
    dispatch(logoutClear());
    this.setState({ userLoggedIn: false, loginData: {} });
    // this.state.userLoggedIn = false;
    this.props.navigation.navigate("Login");
  };
  render() {
    const { name, location, email } = this.state;
    return (
      <>
        <Container>
          <Content>
            <List>
              <ListItem itemDivider>
                <Left>
                  <Text>Personal Information</Text>
                </Left>
                <Body />
                <Button
                  onPress={() => this.props.navigation.navigate("Editpi")}
                  transparent
                  danger
                >
                  <Icon name="edit" type="AntDesign" />
                </Button>
              </ListItem>
              <ListItem>
                <Text>
                  {" "}
                  <Icon
                    style={{ fontSize: 16 }}
                    name="user"
                    type="AntDesign"
                  />{" "}
                  {name}
                </Text>
              </ListItem>
              <ListItem>
                <Text>
                  {" "}
                  <Icon
                    style={{ fontSize: 16 }}
                    name="mail"
                    type="AntDesign"
                  />{" "}
                  {email}
                </Text>
              </ListItem>
              <ListItem>
                <Text>
                  {" "}
                  <Icon
                    style={{ fontSize: 16 }}
                    name="marker"
                    type="Foundation"
                  />{" "}
                  {location}
                </Text>
              </ListItem>
              <ListItem itemDivider>
                <Left>
                  <Text>Interest's </Text>
                </Left>
                <Body />
                <Button
                  onPress={() => this.props.navigation.navigate("Editpi")}
                  transparent
                  danger
                >
                  <Icon name="edit" type="AntDesign" />
                </Button>
              </ListItem>
              <ListItem>
                {Object.keys(this.state.interests).map(intr => {
                  return (
                    <Badge key={intr} value={intr}>
                      <Text style={{ color: "white" }}>
                        {this.state.interests[intr].name}
                      </Text>
                    </Badge>
                  );
                })}
              </ListItem>

              <ListItem itemDivider>
                <Text>Profile Image</Text>
              </ListItem>
              <ListItem>
                <ListItem>
                  {/* <View style={styles.inline}> */}
                  <Button
                    iconLeft
                    transparent
                    bordered
                    danger
                    onPress={this.toggleModal}
                  >
                    <Icon name="folder-images" type="Entypo" />
                    <Text>Gallery</Text>
                  </Button>
                  <Button
                    iconLeft
                    transparent
                    bordered
                    danger
                    onPress={this.toggleModal}
                  >
                    <Icon name="camerao" type="AntDesign" />
                    <Text>Camera</Text>
                  </Button>
                </ListItem>
              </ListItem>
            </List>
            <Button
              transparent
              danger
              iconLeft
              onPress={() => this.clearAsyncStorage()}
            >
              <Icon name="logout" type="AntDesign" />
              <Text>Logout</Text>
            </Button>
            {/* <View style={{ flex: 1 }}>
              <Modal
                isVisible={this.state.isModalVisible}
                coverScreen={false}
                backdropOpacity={1}
                backdropColor="white"
              >
                <View style={{ flex: 1 }}>
                  <Text>Hello!</Text>
                  <Button title="Hide modal" onPress={this.toggleModal} />
                </View>
              </Modal>
            </View> */}
          </Content>

          {/* <Modal
            animationType="fade"
            transparent={false}
            presentationStyle={"formSheet"}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              Alert.alert("Modal has been closed.");
            }}
          >
            <View style={{ marginTop: 22 }}>
              <View>
                <Text>Hello World!</Text>

                <TouchableHighlight
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}
                >
                  <Text>Hide Modal</Text>
                </TouchableHighlight>
              </View>
            </View>
          </Modal> */}
        </Container>

        {/* <ScrollView>
          <Container>
            <Container style={styles.regForm}>
              <TextInput
                style={styles.textimp}
                placeholder="Name"
                name="name"
                value={name}
                onChangeText={name => this.setState({ name })}
                placeholderTextColor="grey"
              />
              <TextInput
                style={styles.textimp}
                placeholder="Bio"
                name="bio"
                value={bio}
                onChangeText={bio => this.setState({ bio })}
                placeholderTextColor="grey"
              />
              <TextInput
                style={styles.textimp}
                placeholder="Location"
                name="location"
                value={location}
                onChangeText={location => this.setState({ location })}
                placeholderTextColor="grey"
              />
              <TextInput
                style={styles.textimp}
                placeholder="Email"
                name="email"
                value={email}
                onChangeText={email => this.setState({ email })}
                placeholderTextColor="grey"
              />
              <TouchableOpacity
                style={styles.button}
                block
                light
                onPress={this.updateUserProfile}
              >
                <Text style={{ color: "black", fontSize: 25 }}>Update</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.button}
                block
                light
                onPress={() => this.props.navigation.navigate("Interests")}
              >
                <Text style={{ color: "black", fontSize: 25 }}>
                  Edit Interests
                </Text>
              </TouchableOpacity>

              <View style={styles.inline}>
                <Button rounded danger style={{ backgroundColor: "red" }}>
                  <Icon name="add" />
                </Button>
                <Text style={{ color: "black", marginTop: 7, marginLeft: 4 }}>
                  Add up to three images
                </Text>
              </View>

              <View>
                <Button title="+" />
              </View>
            </Container>
            <Fab
              direction="up"
              containerStyle={{}}
              style={{ backgroundColor: "#FF0000" }}
              position="bottomRight"
              onPress={this.submitProfile}
            >
              <Icon name="arrow-forward" />
            </Fab>
          </Container>
        </ScrollView> */}
        <FooterTabs navigation={this.props.navigation} />
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FF0000"
  },
  head: {
    // height: 140,
    backgroundColor: "#cc0000"
  },
  textimp: {
    color: "grey",
    fontSize: 20,
    borderColor: "#efefef",
    borderWidth: 2,
    borderRadius: 25,
    padding: 13,
    marginTop: 15
  },
  spinnerTextStyle: {
    color: "#FF0000"
  },
  container1: {
    flex: 1,
    justifyContent: "center"
  },
  horizontal: {
    flexDirection: "row",
    justifyContent: "space-around",
    padding: 10
  },
  logo: {
    height: 40,
    width: 120
  },
  // button: {
  //   borderRadius: 25,
  //   alignItems: "center",
  //   backgroundColor: "#fff",
  //   borderWidth: 2,
  //   borderColor: "#cc0000",
  //   padding: 10,
  //   marginTop: 30,
  //   fontSize: 20
  // },
  button: {
    borderRadius: 25,
    alignItems: "center",
    backgroundColor: "#efefef",
    borderWidth: 1,
    borderColor: "darkgrey",
    padding: 10,
    marginTop: 30,
    fontSize: 20
  },
  // textimp: {
  //   color: "red",
  //   fontSize: 20,
  //   borderColor: "red",
  //   borderWidth: 3,
  //   borderRadius: 8,
  //   padding: 10,
  //   marginTop: 15
  // },
  textcity: {
    width: "80%",
    color: "red",
    fontSize: 20,
    borderColor: "red",
    borderWidth: 3,
    borderRadius: 8,
    padding: 10,
    marginTop: 15
  },
  regForm: {
    padding: 30,
    marginTop: 40
  },
  inline: {
    marginTop: 15,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  },
  inlinecity: {
    marginTop: 2,
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row"
  }
});

const mapStateToProps = state => {
  return {
    loginData: state.user.loginData,
    userLoggedIn: state.user.userLoggedIn
  };
};

export default connect(mapStateToProps)(Profile);
