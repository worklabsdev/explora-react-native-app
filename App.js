/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  StatusBar,
  YellowBox
} from "react-native";

import store from "./src/store";
import { Provider } from "react-redux";
import {
  createStackNavigator,
  createAppContainer,
  HeaderStyleInterpolator
} from "react-navigation";
import Login from "./src/components/Login";
import { Root } from "native-base";
import Splash from "./src/components/Splash";
import Register from "./src/components/Register";
import Profile from "./src/components/Profile";
import Interests from "./src/components/Interests";
import Activities from "./src/components/Activities";
import AddActivity from "./src/components/Addactivity";
import Myactivity from "./src/components/Myactivity";
import Activityr from "./src/components/Activityr";
import Editpi from "./src/components/Editpi";
import Myrequests from "./src/components/Myrequests";

// const instructions = Platform.select({
//   ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
//   android:
//     "Double tap R on your keyboard to reload,\n" +
//     "Shake or press menu button for dev menu"
// });

// const MainNavigator = createStackNavigator({
//   Home: { screen: HomeScreen },
//   Profile: { screen: ProfileScreen }
// });

const Gg = () => {
  return (
    <>
      <Text>sds</Text>
    </>
  );
};
const nOption = {
  title: "Add Activity",
  headerTintColor: "#ffffff",
  headerStyle: {
    backgroundColor: "#cc0000",
    borderBottomColor: "#ffffff"
  },
  headerTitleStyle: {
    color: "#FFFFFF"
  }
};

const MainNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      header: null
    }
  },
  Register: {
    screen: Register,
    navigationOptions: {
      title: "REGISTER",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
    // navigationOptions: {
    //   header: null
    // }
  },
  Profile: {
    screen: Profile,
    navigationOptions: {
      title: "YOUR ACCOUNT",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  },
  Interests: {
    screen: Interests,
    navigationOptions: {
      title: "INTEREST'S",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  },
  AddActivity: {
    screen: AddActivity,
    navigationOptions: {
      title: "ADD ACTIVITY",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  },
  Activities: {
    screen: Activities,

    navigationOptions: {
      title: "ACTIVITY",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  },
  Myactivity: {
    screen: Myactivity,
    navigationOptions: {
      header: null
    }
    // navigationOptions: {
    //   title: "MY ACTIVITY",
    //   headerTintColor: "#ffffff",
    //   headerStyle: {
    //     backgroundColor: "#cc0000",
    //     borderBottomColor: "#ffffff"
    //   },
    //   headerTitleStyle: {
    //     color: "#FFFFFF"
    //   }
    // }
  },
  Activityr: {
    screen: Activityr,
    navigationOptions: {
      title: "REQUESTS",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  },
  Editpi: {
    screen: Editpi,
    navigationOptions: {
      title: "EDIT PERSONAL INFORMATION",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  },
  Myrequests: {
    screen: Myrequests,
    navigationOptions: {
      title: "My Requests",
      headerTintColor: "#ffffff",
      headerStyle: {
        backgroundColor: "#cc0000",
        borderBottomColor: "#ffffff"
      },
      headerTitleStyle: {
        color: "#FFFFFF"
      }
    }
  }
  // UserRequests: {
  //   screen: UserRequests,
  //   navigationOptions: {
  //     title: "REQUESTS",
  //     headerTintColor: "#ffffff",
  //     headerStyle: {
  //       backgroundColor: "#cc0000",
  //       borderBottomColor: "#ffffff"
  //     },
  //     headerTitleStyle: {
  //       color: "#FFFFFF"
  //     }
  //   }
  // }
});

let Navigation = createAppContainer(MainNavigator);
YellowBox.ignoreWarnings(["ViewPagerAndroid", "Slider"]);
class App extends Component {
  state = {
    ready: false
  };
  componentDidMount() {
    setTimeout(() => {
      this.setState({ ready: true });
    }, 0);
  }

  render() {
    return (
      <Provider store={store}>
        {/* <Activities /> */}
        {/* <Interests /> */}
        {/* <AddActivity /> */}
        {/* <Activityr /> */}
        <Root>
          <StatusBar backgroundColor="red" barStyle="light-content" />
          {this.state.ready ? <Navigation /> : <Splash />}
        </Root>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});

export default App;
// export default App;
